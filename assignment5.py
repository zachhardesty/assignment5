#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

# -----------
# imports
# -----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [
    {"title": "Software Engineering", "id": "1"},
    {"title": "Algorithm Design", "id": "2"},
    {"title": "Python", "id": "3"},
]


# NOTE: no err checking
def getBook(id: int):
    """
    filter for book with id matching arg 1
    """
    return [book for book in books if book["id"] == id][0]


@app.route("/", methods=["POST", "GET"])
@app.route("/book/", methods=["POST", "GET"])
def showBook():
    if request.method == "POST":
        # process based on action passed
        if request.form["action"] == "UPDATE":
            book = getBook(request.form["id"])
            book["title"] = request.form["title"]

        elif request.form["action"] == "DELETE":
            book = getBook(request.form["id"])
            books.remove(book)

        elif request.form["action"] == "CREATE":
            books.append(
                {"title": request.form["title"], "id": f"{random.randint(4, 2 ** 16)}"}
            )

    return render_template("showBook.html", books=books)


@app.route("/book/JSON/")
def bookJSON():
    return jsonify(books)


@app.route("/book/new/", methods=["GET", "POST"])
def newBook():
    return render_template("newBook.html")


@app.route("/book/<string:book_id>/edit/", methods=["GET", "POST"])
def editBook(book_id):
    return render_template("editBook.html", book=getBook(book_id))


@app.route("/book/<string:book_id>/delete/", methods=["GET", "POST"])
def deleteBook(book_id):
    return render_template("deleteBook.html", book=getBook(book_id))


if __name__ == "__main__":
    app.debug = True
    app.run(host="127.0.0.1", port=5000)
